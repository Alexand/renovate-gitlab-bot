export const RENOVATE_PROJECT_ID = "51754613"; // Changed to 'Alexand/renovate-gitlab-bot'

// FORCING DRY_RUN on the first push
// export const DRY_RUN = (process.env.DRY_RUN ?? "true") === "true";
export const DRY_RUN = "true" === "true";
export const RENOVATE_BOT_USER = "Alexand"; // Changed to my personal user
export const RENOVATE_STOP_UPDATING_LABEL = "automation:bot-no-updates";
