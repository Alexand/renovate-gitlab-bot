const {
  createServerConfig,
  baseConfig,
  epBaseConfig,
  // availableRouletteReviewerByRole,
} = require("../lib/shared");

module.exports = createServerConfig([
  {
    repository: "alexand-group/test-renovate-group/omnibus-gitlab",
    ...baseConfig,
    labels: [...epBaseConfig.labels, "workflow::ready for review"],
    // reviewers: availableRouletteReviewerByRole("gitlab-development-kit"),
    // reviewersSampleSize: 1,
    enabledManagers: ["bundler"],
    reviewers: ["Alexand"],
    packageRules: [
      {
        matchPackagePatterns: [".+"],
        excludePackagePatterns: ["^thor"],
        rangeStrategy: "bump",
        matchManagers: ["bundler"],
        groupName: "Ruby dependencies",
      },
    ],
  },
]);
